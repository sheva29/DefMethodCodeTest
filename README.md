# Def Method Code Test
Java Commandline application that parses 3 different inputs, sorts and generates3 different outputs based on arguments

#### Setup

* Make sure Java JDK is installed in order to run Java from the command line http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


#### Instruction running from command line

1. Unzip file which should contain DefMethodCodeTest.jar and DefMethodCodeTest_lib folder

2. In terminal(mac) or commandline(windows), cd to the file location of the unzipped files

3. In order to run the Java application type:
 
	`java -jar DefMethodCodeTest.jar`

4. Add arguments to see different outputs:
	
	`java -jar DefMethodCodeTest.jar output1`
	
	`java -jar DefMethodCodeTest.jar output2`

	`java -jar DefMethodCodeTest.jar output3`

#### Main GitLab Repo
https://gitlab.com/sheva29/DefMethodCodeTest/tree/master

#### Java Files
https://gitlab.com/sheva29/DefMethodCodeTest/tree/master/src/net/codejava



package net.codejava;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

//Classes
import net.codejava.UserDataController;

public class Solution {

	public static UserDataController Controller = new UserDataController();	

	public static void main(String[] args) throws IOException {		

		if(readDataFromFile()){
			Controller.generateViewWithArgs(args);
		}
	}	

	public static boolean readDataFromFile() throws IOException{	
		
		String target_dir = "./DefMethodCodeTest_lib";
		File dir = new File(target_dir);
		File[] files = dir.listFiles();
		BufferedReader inputStream = null;
		
		if(files.length > 0){
			for (File f: files){			
				if(f.isFile()){
					try{
						inputStream = new BufferedReader(new FileReader(f));
						String line;
						while((line = inputStream.readLine()) != null){
							Controller.createUser(line);
						}
					}
					catch(IOException exc){
						exc.getMessage();
						return false;
					}
					finally{
						if (inputStream != null){
							inputStream.close();						
						}					
					}
				}else{
					System.out.println("Error: Data not found or empty file. Make sure file path is right");
					return false;				
				}
			}
			return true;
		}else{
			
			System.out.printf("Warning - Folder %s is empty. Make sure to include files", target_dir);
			System.out.println();
			return false;
		}		
	}	

}

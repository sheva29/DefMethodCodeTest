package net.codejava;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import net.codejava.UserData;
import net.codejava.UserDataView;

public class UserDataController {
	
	UserDataController(){};
	
	public ArrayList<UserData> UserEntries = new ArrayList<UserData>();
	public UserDataView DataView = new UserDataView();
	
	public void createUser(String entry){
		
		SimpleDateFormat dob = new SimpleDateFormat("MM/dd/yyyy");
		
		if (entry.indexOf(",") > 0) {
			entry = entry.replaceAll("\\s", "");
			String[] single = entry.split(",");
			UserData user = new UserData();
			user.LastName = single[0];
			user.FirstName = single[1];
			user.Gender = single[2];
			user.FavoriteColor = single[3];			
			try {
				user.DateOfBirth = dob.parse(single[4]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			UserEntries.add(user);

		}else if(entry.indexOf("|") > 0) {
			
			entry = entry.replaceAll("\\s", "");
			String[] single = entry.split("\\|");
			UserData user = new UserData();
			user.LastName = single[0];
			user.FirstName = single[1];
			user.MiddleInitial = single[2];
			user.Gender = (single[3].contains("M")) ? "Male" : "Female";
			user.FavoriteColor = single[4];
			try {
				user.DateOfBirth = dob.parse(single[5].replace("-", "/"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			UserEntries.add(user);

		}else{

			String[] single = entry.split(" ");
			UserData user = new UserData();
			user.LastName = single[0];
			user.FirstName = single[1];
			user.MiddleInitial = single[2];
			user.Gender = (single[3].contains("M")) ? "Male" : "Female";
			try {
				user.DateOfBirth = dob.parse(single[4].replace("-", "/"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			user.FavoriteColor = single[5];
			UserEntries.add(user);
		}
	}
	
	public void generateViewWithArgs(String[] _args) throws IOException{
		
		boolean isRightArgument = true;
		if (_args.length > 0){
			//we sort data		
			switch(_args[0]){
				case "output1":					
					sortGenderLastName();
					break;
					
				case "output2":					
					sortDOBLastName();				
					break;
				
				case "output3":					
					sortLastNameReversed();
					break;
				
				default:
					isRightArgument = false;
					System.out.println("Ilegal Argument: " + _args[0] + " - Only valid arguments: 'output1', 'output2' or 'output3");					
			}
			
			//we display data
			if(isRightArgument){
				
				for(UserData user: UserEntries){
					DataView.displayUserData(user);
				}
			}
			
		}else{
			
			System.out.println("Warning - In order to display data out type: 'output1', 'output2' or 'output3");
		}
	}	
	
	private void sortGenderLastName(){
		
		Comparator<UserData> output1 = Comparator.comparing((UserData u1) -> u1.Gender).thenComparing((UserData u1) -> u1.LastName);
		Collections.sort(UserEntries, output1);
	}
	
	private void sortDOBLastName(){
		
		Comparator<UserData> output2 = Comparator.comparing((UserData u1) -> u1.DateOfBirth).thenComparing((UserData u1) -> u1.LastName);
		Collections.sort(UserEntries, output2);
	}
	
	private void sortLastNameReversed(){
		
		Comparator<UserData> output3 = Comparator.comparing((UserData u1) -> u1.LastName).reversed();
		Collections.sort(UserEntries, output3);		
	}		
}

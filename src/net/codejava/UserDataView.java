package net.codejava;
import java.text.SimpleDateFormat;

public class UserDataView {

	public UserDataView(){};
	
	public void displayUserData(UserData User){	
		
		String DateOfBirthFormatted = new SimpleDateFormat("M/d/yyy").format(User.DateOfBirth);
		System.out.printf("%s %s %s %s %s", User.LastName, User.FirstName, User.Gender, DateOfBirthFormatted, User.FavoriteColor);
		System.out.println();
	}
}
